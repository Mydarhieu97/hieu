﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DropShipping.Startup))]
namespace DropShipping
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
